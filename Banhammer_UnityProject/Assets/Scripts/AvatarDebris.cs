﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarDebris : MonoBehaviour {

	Vector3 contactPos;

	[SerializeField]
	Material sparksMaterial;

	public void Init(Vector3 _contactPos, Color _uniqueColour)
	{
		sparksMaterial.SetColor("_EmissionColor", _uniqueColour * 3.0f);
		contactPos = _contactPos;
	}

	void Start ()
	{
		Rigidbody[] rigidbodies = GetComponentsInChildren<Rigidbody>();

		for (int i = 0; i < rigidbodies.Length; i++)
		{
			Rigidbody body = rigidbodies[i];
			body.AddForce((body.position - contactPos) * 300.0f);
		}
	}
}
